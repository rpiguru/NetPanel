import os
import sys
import time
from mpu6050 import mpu6050

_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


from utils.common import rotate_display


class MPU6050:

    def __init__(self):
        self.sensor = None
        self._last_print_time = 0

    def read_sensor_data(self):
        if self.sensor is None:
            try:
                self.sensor = mpu6050(address=0x68)
            except Exception as e:
                if time.time() - self._last_print_time > 60:
                    print(f'MPU6050: Failed to open sensor - {e}')
                    self._last_print_time = time.time()
                return
        try:
            data = dict(
                temp=round(self.sensor.get_temp(), 3),
                accel={k: round(v, 3) for k, v in self.sensor.get_accel_data().items()},
                gyro={k: round(v, 3) for k, v in self.sensor.get_gyro_data().items()},
            )
            return data
        except Exception as e:
            if time.time() - self._last_print_time > 60:
                print(f'MPU6050: Failed to read sensor data - {e}')
                self._last_print_time = time.time()
            self.sensor = None


def check_gyroscope():
    """
    Upright/Neutral Position: x= 0 y= -10               => landscape
    Turning it to the right position: x= -10  y= 0      => portrait rightwards
    Turning it to the left position x= 10  y= 0         => portrait leftwards
    Upside-down: x= 0 y=10                              => landscape inverted vertically
    """
    mpu = MPU6050()
    while True:
        data = mpu.read_sensor_data()
        if data:
            g = data['gyro']
            if g['x'] > 7 and abs(g['y']) < 3:
                rotate_display(90)
            elif abs(g['x']) < 3:
                if g['y'] > 7:
                    rotate_display(180)
                elif g['y'] < -7:
                    rotate_display(0)
            elif g['x'] < -7 and abs(g['y']) < 3:
                rotate_display(270)
        time.sleep(.5)


if __name__ == '__main__':

    m = MPU6050()

    while True:
        try:
            print(m.read_sensor_data())
            time.sleep(1)
        except KeyboardInterrupt:
            break
