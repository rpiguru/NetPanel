import gc
import json
import os
import signal
import subprocess
import sys
import threading
import time

import netifaces
import platform
import http.client
from pymediainfo import MediaInfo


_cur_dir = os.path.dirname(os.path.realpath(__file__))
_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


from utils.settings import CONFIG_FILE, INTERNET_CHECK_URL, INTERNET_CHECK_TIMEOUT, WIFI_COUNTRY_CODE
from utils.logger import logger


try:
    is_rpi = 'arm' in os.uname()[4]
except (AttributeError, IndexError):
    is_rpi = False


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi:
        try:
            cpuserial = "0000000000000000"
            f = open('/proc/cpuinfo', 'r')
            for line in f:
                if line[0:6] == 'Serial':
                    cpuserial = line[10:26].lstrip('0')
            f.close()
            return cpuserial
        except Exception as e:
            print(e)
    return '12345678'


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    is_running = False
    cmd = "ps -aef | grep -i '%s' | grep -v 'grep' | awk '{ print $3 }'" % proc_name
    try:
        if len(os.popen(cmd).read().strip().splitlines()) > 0:
            is_running = True
    except Exception as e:
        print(f'Failed to get status of the process({proc_name}) - {e}')
    return is_running


def get_ip_address(ifname='wlan0'):
    """
    Get assigned IP address of given interface
    :param ifname: interface name such as wlan0, eth0, etc
    :return: If not on RPi, returns the IP address of LAN
    """
    if platform.system() == 'Windows':
        return '192.168.1.170'
    try:
        return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except Exception as e:
        print(f'Failed to get IP address of {ifname}, reason: {e}')


def get_current_ap():
    """
    Get SSID of the current AP
    :return:
    """
    if is_rpi:
        pipe = os.popen('/sbin/iwgetid -r')
        ap = pipe.read().strip()
        pipe.close()
        return ap
    else:
        return "Testing AP"


def get_mac_address(interface='wlan0'):
    try:
        mac_str = open('/sys/class/net/' + interface + '/address').read()
    except Exception as e:
        logger.error(f'Failed to get MAC address of {interface}: {e}')
        mac_str = "00:00:00:00:00:00"
    return mac_str[0:17]


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_free_gpu_size():
    gc.collect()
    if is_rpi:
        try:
            pipe = os.popen('sudo vcdbg reloc stats | grep "free memory"')
            data = pipe.read().strip()
            pipe.close()
            return data
        except OSError:
            logger.error('!!! Failed to get free GPU size! ')
            tot, used, free = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])
            logger.error(f'Total: {tot}, Used: {used}, Free: {free}')
    return 0


def disable_screen_saver():
    if is_rpi:
        os.system('sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')


last_log_time = 0


def check_internet_connection():
    """
    Check internet connection
        It will be faster to just make a HEAD request so no HTML will be fetched.
        Also I am sure google would like it better this way :)
    """
    global last_log_time
    conn = http.client.HTTPConnection(INTERNET_CHECK_URL, timeout=INTERNET_CHECK_TIMEOUT)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except Exception as e:
        if time.time() - last_log_time > 60:
            logger.error(f"Failed to establish connection with {INTERNET_CHECK_URL} - {e}")
            last_log_time = time.time()
        conn.close()
        return False


def kill_process_by_name(proc_name, args="", use_sudo=True, sig=None):
    sig = sig if sig is not None else signal.SIGKILL
    p = subprocess.Popen(['ps', '-Af'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line and (not args or args in line):
            pid = int(line.split()[1])
            logger.info(f'Found PID({pid}) of `{proc_name}`, killing with {sig}...')
            try:
                if use_sudo:
                    os.system(f'sudo kill -{sig} {pid}')
                else:
                    os.kill(pid, sig)
            except Exception as e:
                logger.error(f"Failed to kill {proc_name} - {e}")


def rotate_display(angle):
    val = {0: "normal", 90: "left", 180: "inverted", 270: "right"}[angle]
    cmd = f"DISPLAY=:0 xrandr --output HDMI-1 --rotate {val}"
    os.system(cmd)


def update_dict_recursively(dest, updated):
    """
    Update dictionary recursively.
    :param dest: Destination dict.
    :type dest: dict
    :param updated: Updated dict to be applied.
    :type updated: dict
    :return:
    """
    for k, v in updated.items():
        if isinstance(dest, dict):
            if isinstance(v, dict):
                r = update_dict_recursively(dest.get(k, {}), v)
                dest[k] = r
            else:
                dest[k] = updated[k]
        else:
            dest = {k: updated[k]}
    return dest


def get_config():
    if os.path.exists(CONFIG_FILE):
        return json.loads(open(CONFIG_FILE).read())
    else:
        return {}


def update_config_file(data):
    old_data = update_dict_recursively(dest=get_config(), updated=data)
    with open(CONFIG_FILE, 'w') as jp:
        json.dump(old_data, jp, indent=2)


def create_wifi_config(ssid, password):
    config_lines = [
        f'country={WIFI_COUNTRY_CODE}',
        'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev',
        'update_config=1'
        '\n',
        'network={',
        '\tssid="{}"'.format(ssid),
        '\tpsk="{}"'.format(password),
        '\tkey_mgmt=WPA-PSK',
        '}'
    ]
    config = '\n'.join(config_lines)

    if is_rpi:
        with open("/tmp/wifi.conf", "w") as wifi:
            wifi.write(config)
        os.system("sudo mv /tmp/wifi.conf /etc/wpa_supplicant/wpa_supplicant.conf")
    else:
        print(config)


def scan_wifi_networks():
    if is_rpi:
        try:
            iwlist_raw = subprocess.Popen(['sudo', '/sbin/iwlist', 'wlan0', 'scan'], stdout=subprocess.PIPE)
            ap_list, err = iwlist_raw.communicate()
            ap_array = []
            for line in ap_list.decode('utf-8').rsplit('\n'):
                if 'ESSID' in line:
                    ap_ssid = line[27:-1]
                    if ap_ssid != '' and ap_ssid not in ap_array and "\\x" not in ap_ssid:
                        ap_array.append(ap_ssid)
            return sorted(ap_array)
        except Exception as e:
            logger.error(f"Failed to scan wifi: {e}")
            return []
    else:
        return [f'AP {i}' for i in range(10)]


def check_config_exists():
    conf = get_config()
    if conf.get('mode') is None:
        logger.info("Not configured yet!")
        if is_rpi:
            kill_process_by_name('fbi', sig=signal.SIGTERM, use_sudo=True)
            _img_path = os.path.join(_cur_dir, 'images', 'not_configured.png')
            subprocess.Popen(f"bash {os.path.join(_par_dir, 'scripts', 'show_not_configured.sh')}", shell=True)
        return False
    return True


def get_screen_resolution():
    """
    Get resolution of the screen
    :return:
    """
    if is_rpi:
        pipe = os.popen('fbset -s')
        data = pipe.read().strip()
        pipe.close()
        for line in data.splitlines():
            if line.startswith('mode'):
                w, h = [int(p) for p in line.split('"')[1].split('x')]
                return w, h
    else:
        return 1280, 720


def is_video_file(file_path):
    media_info = MediaInfo.parse(file_path)
    for track in media_info.tracks:
        if track.track_type == 'Video':
            return True


def delay_and_reboot():
    time.sleep(2)
    os.system('sudo reboot')


def start_ap():
    if is_rpi:
        threading.Thread(target=_start_ap).start()


def _start_ap():
    p = subprocess.Popen(f"bash {os.path.join(_par_dir, 'scripts', 'wifi_set_ap.sh')}", shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    logger.info(f"OUT: {stdout}")
    logger.info(f"ERR: {stderr}")


def start_station():
    if is_rpi:
        threading.Thread(target=_start_station).start()


def _start_station():
    logger.info("Switching to STA mode...")
    p = subprocess.Popen(f"bash {os.path.join(_par_dir, 'scripts', 'wifi_set_sta.sh')}", shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    logger.info(f"OUT: {stdout}")
    logger.info(f"ERR: {stderr}")


def turn_display_off():
    os.system("xset -display :0 dpms force off")


def turn_display_on():
    os.system("xset -display :0 dpms force on && xrefresh")


def kill_browser():
    kill_process_by_name('unclutter')
    kill_process_by_name('chromium-browser')


def drop_cache():
    os.system("sudo sh -c \"echo 1 > /proc/sys/vm/drop_caches\"")
    os.system("sudo sh -c \"echo 2 > /proc/sys/vm/drop_caches\"")
    os.system("sudo sh -c \"echo 3 > /proc/sys/vm/drop_caches\"")


def check_git_commit():
    """
    Compare local/remote commit datetime, and pull if any updates in the remote repo.
    :return:
    """
    p = subprocess.Popen('/bin/bash {}/git_update.sh'.format(_cur_dir), shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    p.wait()

    return stdout.decode(), stderr.decode()
