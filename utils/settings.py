import os
import shutil

_cur_dir = os.path.dirname(os.path.realpath(__file__))


ROOT_DIR = os.path.expanduser('~/.np')
os.makedirs(ROOT_DIR, exist_ok=True)


SERVER_HOST = '10.1.1.1'
SERVER_PORT = 80
DEBUG = False

NETPANEL_URL = "https://www.raspberrypi.org/"

CONFIG_FILE = os.path.join(ROOT_DIR, 'config.json')
if not os.path.exists(CONFIG_FILE):
    shutil.copy(os.path.join(_cur_dir, 'default_config.json'), CONFIG_FILE)

MODE_SELECT_TIMEOUT = 10
PIN_SELECT_TIMEOUT = 10
PATH_SELECT_TIMEOUT = 10

MODES = ['netPanel', 'PC MODE', 'PHOTO MODE', 'MEDIA MODE']


INTERNET_CHECK_URL = 'www.google.com'
INTERNET_CHECK_TIMEOUT = 5

USB_PATH = '/media/usb*'

PHOTO_CMD = "feh -Z -F -x -D 5 -Y -B black -q --auto-rotate -r \"{path}\""
VIDEO_CMD = "vlc --playlist-autostart --quiet --playlist-tree -f --no-qt-privacy-ask " \
            "--no-qt-error-dialogs --loop \"{path}/\""
BROWSER_CMD = "chromium-browser --disable-infobars --incognito --disable-plugins --process-per-site " \
              "--disable-session-crashed-bubble --noerrors --noerrdialogs --check-for-update-interval=31536000 " \
              "--disable-gpu --disable-software-rasterizer --disable-suggestions-service --disable-translate " \
              "--simulate-outdated-no-au='Tue, 31 Dec 2099 23:59:59 GMT' --disable-component-update --kiosk {url}"

WIFI_COUNTRY_CODE = "AU"

try:
    from local_settings import *
except ImportError:
    pass
