echo "Returning to the STA mode"
sudo cp /etc/dhcpcd.conf.sta /etc/dhcpcd.conf
sudo cp /etc/dnsmasq.conf.sta /etc/dnsmasq.conf

sudo service hostapd stop
sudo service dnsmasq restart
sudo service dhcpcd restart
