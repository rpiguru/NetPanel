
# Remove SSH warning
sudo apt purge -y libpam-chksshpwd

# Remove login prompt
sudo systemctl disable getty@tty1

# Disable booting logo and messages
sudo echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo systemctl mask plymouth-start.service
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/g" /boot/cmdline.txt

# Disable some services to reduce booting time
sudo systemctl disable hciuart
sudo echo "dtoverlay=pi3-disable-bt" | sudo tee -a /boot/config.txt
sudo echo "boot_delay=0" | sudo tee -a /boot/config.txt

sudo mkdir /etc/systemd/system/networking.service.d
sudo touch /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "[Service]" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "TimeoutStartSec=1" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
sudo rm /etc/systemd/system/dhcpcd.service.d/wait.conf

# Disable screen saver
sudo echo "xset s off" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart
sudo echo "xset -dpms" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart

# Disable welcome wizard
sudo rm /etc/xdg/autostart/piwiz.desktop
