echo "Setting AP mode now..."
sudo cp /etc/dhcpcd.conf.ap /etc/dhcpcd.conf
sudo cp /etc/dnsmasq.conf.ap /etc/dnsmasq.conf
sudo service dnsmasq restart
sudo service dhcpcd restart
sudo service hostapd restart
echo "Done"
