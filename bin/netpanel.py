import gc
import threading
import traceback
import os
import signal
import subprocess
import sys
import time
from pynput import keyboard
import qdarkstyle
from PySide2.QtCore import Qt, QTimer, QSize, Signal
from PySide2.QtWidgets import QVBoxLayout, QSystemTrayIcon, QStyle, QApplication, QDialog

from utils.settings import BROWSER_CMD
from utils.common import get_config, is_rpi, check_internet_connection, kill_process_by_name, get_screen_resolution, \
    check_running_proc, kill_browser, start_station, drop_cache, check_git_commit
# from utils.gyro import check_gyroscope
from utils.logger import logger
from widgets.loading_indicator import LoadingIndicator
from widgets.media_config import MediaConfigWidget
from widgets.mode_config import ModeConfigWidget
from widgets.pin_config import PINConfigWidget
from widgets.url_config import URLConfigWidget
from widgets.wifi_config import WiFiConfigWidget


_cur_dir = os.path.dirname(os.path.realpath(__file__))


WIDGETS = {
    "mode": ModeConfigWidget,
    'url_config': URLConfigWidget,
    'pin_config': PINConfigWidget,
    'media_config': MediaConfigWidget,
    'wifi_config': WiFiConfigWidget,
}

w, h = get_screen_resolution()


class NetPanelApp(QDialog):

    _internet_connected = True
    _internet_status = Signal()
    _wifi_connect_time = -1
    _key_released = Signal(object)

    def __init__(self, parent=None):
        super(NetPanelApp, self).__init__(parent)
        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(100, 100, 100, 100)
        self.setGeometry(0, 0, w, h)
        self.setLayout(self.layout)
        self.setMinimumSize(QSize(480, 80))
        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(self.style().standardIcon(QStyle.SP_ComputerIcon))

        if is_rpi:
            self.setWindowFlag(Qt.FramelessWindowHint)
            self.showFullScreen()

        self._cur_widget = None
        self._cur_widget_name = None
        self._tr_internet = None
        self._tr_stop = threading.Event()
        for func in {'_on_internet_status', '_parse_key_input'}:
            setattr(self, f"NetPanelApp.{func}", getattr(self, func))
        self._internet_status.connect(self._on_internet_status)
        self._internet_required = True
        self._key_released.connect(self._parse_key_input)
        self.switch_widget('mode', en_timer=get_config().get('mode') is not None)

    def switch_widget(self, name, **kwargs):
        if self._cur_widget_name and self._cur_widget_name == name:
            logger.warning(f"{name} is already active now.")
            return
        self.clear_layout()
        self._cur_widget = WIDGETS[name](self, name)
        self.layout.addWidget(self._cur_widget)
        self._cur_widget_name = name
        if name == 'mode':
            if not kwargs.get('en_timer', True):
                self._cur_widget.stop_timer()
            if self._tr_internet is not None:
                self._tr_stop.set()
                self._tr_internet.join(.1)
                self._tr_internet = None
        logger.info(f"=== Switched to {name}")
        gc.collect()

    def start_internet_monitoring(self):
        self._tr_stop.clear()
        self._tr_internet = threading.Thread(target=self._check_internet_connection, daemon=True)
        self._tr_internet.start()

    def open_browser(self):
        logger.info(f"Opening browser - {get_config()['url']}")
        self.setCursor(Qt.BlankCursor)
        self.clear_layout()
        self.layout.addWidget(LoadingIndicator(self))
        drop_cache()
        subprocess.Popen("unclutter -idle 0", shell=True)
        os.system("sed -i 's/\"exited_cleanly\":false/\"exited_cleanly\":true/' ~/.config/chromium/Default/Preferences")
        os.system(
            "sed -i 's/\"exit_type\":\"Crashed\"/\"exit_type\":\"Normal\"/' ~/.config/chromium/Default/Preferences")
        subprocess.Popen(BROWSER_CMD.format(url=get_config()['url']), shell=True)
        QTimer.singleShot(40000, self.hide)

    def clear_layout(self):
        for i in reversed(range(self.layout.count())):
            item = self.layout.itemAt(i)
            if item.widget() is not None:
                item.widget().deleteLater()
        self._cur_widget = None
        self._cur_widget_name = None

    def _check_internet_connection(self):
        while not self._tr_stop.is_set():
            if check_internet_connection():
                if not self._internet_connected:
                    logger.info("Internet is connected! :)")
                    self._internet_connected = True
                    self._wifi_connect_time = -1
                    self._internet_status.emit()
            else:
                if self._internet_connected:
                    logger.info("Internet disconnected!")
                    self._internet_connected = False
                    self._internet_status.emit()
                elif self._wifi_connect_time > 0 and time.time() - self._wifi_connect_time > 30:
                    logger.error("Failed to connect to WiFi! Returning to AP mode now...")
                    self._wifi_connect_time = -1
                    self._internet_status.emit()
            time.sleep(1)

    def save_wifi_connect_time(self):
        self._wifi_connect_time = time.time()

    def hide_me(self):
        self.switch_widget('mode', en_timer=False)
        self.setWindowState(Qt.WindowMinimized)
        if not check_internet_connection():
            start_station()

    def show_me(self):
        self.setGeometry(0, 0, w, h)
        self.show()

    def show_app(self):
        if get_config()['mode'] == 'PC MODE':
            self.hide_me()
        else:
            if not check_running_proc(BROWSER_CMD.split(' ')[0]):
                self.open_browser()
            else:
                self.hide_me()

    def _on_internet_status(self):
        if self._internet_connected:
            self.show_app()
        elif self._internet_required or get_config()['mode'] == 'netPanel':
            logger.warning(f"No Internet! Starting AP now...")
            kill_browser()
            if self._cur_widget_name != 'wifi_config':
                self.switch_widget('wifi_config')
                self.show_me()
            else:
                self._cur_widget.setDisabled(False)

    def disable_internet(self):
        self._internet_required = False

    def is_internet_required(self):
        return self._internet_required

    def _parse_key_input(self, key):
        if self._cur_widget:
            self._cur_widget.on_key_pressed(key)
        else:       # netPanel mode with browser opened
            if key == keyboard.Key.esc:
                self.switch_widget('mode', en_timer=False)
                self.show_me()
                kill_browser()

    def on_keyboard_press(self, key):
        if self.windowState() != Qt.WindowMinimized:
            logger.debug(f'{key} pressed')
            self._key_released.emit(key)

    def keyPressEvent(self, event):
        if event.key() != Qt.Key_Escape:
            super(NetPanelApp, self).keyPressEvent(event)

    def closeEvent(self, event):
        event.ignore()
        self.hide()
        self.tray_icon.showMessage(
            "NetPanel",
            "Application is minimized to Tray",
            QSystemTrayIcon.Information,
            2000
        )


def check_remote_repo():
    while True:
        msg, err = check_git_commit()
        if 'Fast-forward' in msg:
            logger.warning(f'===== Remote repository was updated! Pulled - {msg}')
            logger.info("Rebooting now...")
            os.system('sudo reboot')
        time.sleep(60)


def start_netpanel():

    logger.info("========== Starting NetPanel ==========")

    if not check_internet_connection():
        start_station()
        time.sleep(3)

    # Drop cache to get free memory space
    drop_cache()

    sys._excepthook = sys.excepthook

    def exception_hook(exctype, value, exc_tb):
        logger.error(f'!!!! Crashed! Exctype: {exctype}, Value: {value}, Traceback: {traceback.format_tb(exc_tb)}')
        getattr(sys, "_excepthook")(exctype, value, exc_tb)
        sys.exit(1)

    sys.excepthook = exception_hook

    # multiprocessing.Process(target=check_gyroscope, daemon=True).start()

    # if not check_config_exists():
    #     start_ap()
    #     sys.exit(-1)

    threading.Thread(target=check_remote_repo).start()

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
    np_app = NetPanelApp()
    listener = keyboard.Listener(on_press=np_app.on_keyboard_press)
    listener.start()

    kill_process_by_name(proc_name='fbi', use_sudo=True, sig=signal.SIGTERM)
    np_app.show()
    app.exec_()
