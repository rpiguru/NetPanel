import os
import sys
import traceback
import threading

from flask import Flask, render_template, request, redirect

from utils.settings import SERVER_HOST, SERVER_PORT, DEBUG
from utils.common import scan_wifi_networks, create_wifi_config, update_config_file, start_station, delay_and_reboot
from utils.logger import logger


_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
template_dir = os.path.join(_par_dir, 'templates')
static_dir = os.path.join(_par_dir, 'static')


app = Flask(__name__, template_folder=template_dir, static_folder=static_dir)


@app.route('/index')
def index():
    return render_template("index.html")


@app.route('/wifi_setup')
def wifi_setup():
    ap_list = scan_wifi_networks()
    return render_template("wifi.html", ap_list=ap_list)


@app.route('/set_wifi', methods=['POST'])
def set_wifi():
    data = request.form.to_dict()
    create_wifi_config(ssid=data['ssid'], password=data['password'])
    start_station()
    return "OK", 200


@app.route('/mode_setup')
def mode_setup():
    return render_template("mode.html")


@app.route('/set_mode', methods=['GET'])
def set_mode():
    mode = request.args.get('mode')
    logger.info(f"Server: new mode - {mode}")
    update_config_file({"mode": mode})
    threading.Thread(target=delay_and_reboot).start()
    return "Connecting WiFi...", 200


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>', methods=['GET', "POST"])
def catch_all(path):
    return redirect("http://{}:{}/index".format(SERVER_HOST, SERVER_PORT))


def start_server():

    sys._excepthook = sys.excepthook

    def exception_hook(exctype, value, exc_tb):
        logger.error(f'!!!! Crashed! Exctype: {exctype}, Value: {value}, Traceback: {traceback.format_tb(exc_tb)}')
        getattr(sys, "_excepthook")(exctype, value, exc_tb)
        sys.exit(1)

    sys.excepthook = exception_hook

    logger.info(f"===== Running server on 0.0.0.0:{SERVER_PORT}")
    try:
        app.run(debug=DEBUG, host="0.0.0.0", port=SERVER_PORT)
    except Exception as e:
        logger.error(f"Failed to run server - {e}")
