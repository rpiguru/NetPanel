#!/usr/bin/env bash

echo "Stating NetPanel Installation."
echo "Installation logs are saved to /home/pi/netpanel_install.log"

exec &> >(tee -a "/home/pi/netpanel_install.log")

sed -i -- "s/https:\/\/gitlab.com/https:\/\/gitlab+deploy-token-657575:QydivxxTuhBSFPzFpYc3@gitlab.com/g" .git/config

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
user="$(id -u -n)"

sudo apt update -y

sudo apt install -y python3-dev python3-pip libsdl1.2debian screen python3-smbus vlc feh libmediainfo-dev xdotool unclutter
sudo apt --only-upgrade install -y chromium-browser

# Installing PySide2
sudo apt install -y python3-pyside2.qt3dcore python3-pyside2.qt3dinput python3-pyside2.qt3dlogic \
                    python3-pyside2.qt3drender python3-pyside2.qtcharts python3-pyside2.qtconcurrent \
                    python3-pyside2.qtcore python3-pyside2.qtgui python3-pyside2.qthelp python3-pyside2.qtlocation \
                    python3-pyside2.qtmultimedia python3-pyside2.qtmultimediawidgets python3-pyside2.qtnetwork \
                    python3-pyside2.qtopengl python3-pyside2.qtpositioning python3-pyside2.qtprintsupport \
                    python3-pyside2.qtqml python3-pyside2.qtquick python3-pyside2.qtquickwidgets \
                    python3-pyside2.qtscript python3-pyside2.qtscripttools python3-pyside2.qtsensors \
                    python3-pyside2.qtsql python3-pyside2.qtsvg python3-pyside2.qttest python3-pyside2.qttexttospeech \
                    python3-pyside2.qtuitools python3-pyside2.qtwebchannel python3-pyside2.qtwebsockets \
                    python3-pyside2.qtwidgets python3-pyside2.qtx11extras python3-pyside2.qtxml \
                    python3-pyside2.qtxmlpatterns python3-pyside2uic pyside2-tools
sudo sed -i -- "s/#! \/usr\/bin\/python2/#! \/usr\/bin\/python3/g" /usr/bin/pyside2-uic


sudo pip3 install -U pip setuptools wheel

sudo pip3 install -r ${cur_dir}/requirements.txt

# Increase GPU memory size
sudo echo "gpu_mem=144" | sudo tee -a /boot/config.txt

# Enable I2C
echo "dtparam=i2c_arm=on" | sudo tee -a /boot/config.txt
echo "dtparam=i2c1=on" | sudo tee -a /boot/config.txt

# Clear desktop and install splash service
bash ${cur_dir}/scripts/clear_desktop.sh
bash ${cur_dir}/scripts/install_splash.sh

sudo cp ${cur_dir}/images/not_configured.png /opt/

# Allow port 80 to pi
sudo touch /etc/sysctl.d/50-unprivileged-ports.conf
echo "net.ipv4.ip_unprivileged_port_start=0" | sudo tee -a /etc/sysctl.d/50-unprivileged-ports.conf
sudo sysctl --system

# Auto-mount USB drives
sudo apt install -y pmount
sudo cp ${cur_dir}/scripts/usbstick.rules /etc/udev/rules.d/
sudo cp ${cur_dir}/scripts/usbstick-handler@.service /etc/systemd/system/
sudo cp ${cur_dir}/scripts/cpmount /usr/local/bin/
sudo chmod u+x /usr/local/bin/cpmount

# Install WiFi AP
sudo apt install -y hostapd dnsmasq

sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo rfkill unblock wlan

sudo bash -c 'cat > /etc/default/hostapd' << EOF
DAEMON_CONF="/etc/hostapd/hostapd.conf"
EOF

sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.sta
sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.ap
echo "interface wlan0" | sudo tee -a /etc/dhcpcd.conf.ap
echo "    static ip_address=10.1.1.1/24" | sudo tee -a /etc/dhcpcd.conf.ap
echo "    nohook wpa_supplicant" | sudo tee -a /etc/dhcpcd.conf.ap

sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.sta
sudo bash -c 'cat > /etc/dnsmasq.conf.ap' << EOF
interface=wlan0
dhcp-range=10.1.1.100,10.1.1.150,255.255.255.0,24h
EOF

sudo bash -c 'cat > /etc/hostapd/hostapd.conf' << EOF
ctrl_interface=/var/run/hostapd
ctrl_interface_group=0
interface=wlan0
driver=nl80211
ssid=netPanel
hw_mode=g
channel=11
wmm_enabled=0
macaddr_acl=0
auth_algs=1
wpa=0
EOF

echo "vm.swappiness=10" | sudo tee -a /etc/sysctl.conf

# Increase SWAP size
sudo sed -i -- 's/CONF_SWAPSIZE=100/CONF_SWAPSIZE=500/g' /etc/dphys-swapfile

# Disable update popup
sudo touch /etc/chromium-browser/customizations/01-disable-update-check;echo CHROMIUM_FLAGS=\"\$\{CHROMIUM_FLAGS\} --check-for-update-interval=31536000\" | sudo tee /etc/chromium-browser/customizations/01-disable-update-check

# Enable auto start
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -mS server -d\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S server -X stuff \"cd ${cur_dir////\\/}\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S server -X stuff \"sudo bash scripts\/firewall.sh\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S server -X stuff \"python3 server.py\\\\r\"\'\\nexit 0/g" /etc/rc.local

# Enable Auto Start
sudo cp ${cur_dir}/scripts/netpanel_start.sh /opt/
sudo sed -i -- "s/DIR/${cur_dir////\\/}/g" /opt/netpanel_start.sh

sudo sed -i -- "s/^@lxpanel --profile LXDE-pi/sudo fbi -d \/dev\/fb0 -T 1 -1 --noverbose --fitwidth --autozoom \/opt\/splash.png\\n@lxpanel --profile LXDE-pi/g" /etc/xdg/lxsession/LXDE-pi/autostart

echo "bash /opt/netpanel_start.sh" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart

python3 ${cur_dir}/ui/compile_ui.py

python3 compile.py build_ext --inplace && rm -r build && rm compile.py requirements.txt README.md

echo "Finished Installation. Please reboot now."
