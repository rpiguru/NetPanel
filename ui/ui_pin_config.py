# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/pi/NetPanel/ui/pin_config.ui',
# licensing of '/home/pi/NetPanel/ui/pin_config.ui' applies.
#
# Created: Sun Sep 27 11:14:40 2020
#      by: pyside2-uic  running on PySide2 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_PinConfig(object):
    def setupUi(self, PinConfig):
        PinConfig.setObjectName("PinConfig")
        PinConfig.resize(837, 248)
        self.verticalLayout = QtWidgets.QVBoxLayout(PinConfig)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lb_title = QtWidgets.QLabel(PinConfig)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lb_title.sizePolicy().hasHeightForWidth())
        self.lb_title.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setWeight(75)
        font.setBold(True)
        self.lb_title.setFont(font)
        self.lb_title.setAutoFillBackground(True)
        self.lb_title.setStyleSheet("hover {\n"
"    background-color: rgb(255, 255, 255);\n"
"    color: rgb(0, 0, 0);\n"
"}")
        self.lb_title.setObjectName("lb_title")
        self.verticalLayout.addWidget(self.lb_title)
        self.txt = QtWidgets.QLineEdit(PinConfig)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.txt.setFont(font)
        self.txt.setObjectName("txt")
        self.verticalLayout.addWidget(self.txt)
        self.btnSubmit = QtWidgets.QPushButton(PinConfig)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.btnSubmit.setFont(font)
        self.btnSubmit.setObjectName("btnSubmit")
        self.verticalLayout.addWidget(self.btnSubmit)
        self.btnBack = QtWidgets.QPushButton(PinConfig)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName("btnBack")
        self.verticalLayout.addWidget(self.btnBack)

        self.retranslateUi(PinConfig)
        QtCore.QMetaObject.connectSlotsByName(PinConfig)

    def retranslateUi(self, PinConfig):
        PinConfig.setWindowTitle(QtWidgets.QApplication.translate("PinConfig", "Form", None, -1))
        self.lb_title.setText(QtWidgets.QApplication.translate("PinConfig", "Please input PIN", None, -1))
        self.btnSubmit.setText(QtWidgets.QApplication.translate("PinConfig", "Submit", None, -1))
        self.btnBack.setText(QtWidgets.QApplication.translate("PinConfig", "Back", None, -1))

