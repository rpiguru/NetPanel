import glob
import ntpath
import os
import platform


_cur_dir = os.path.dirname(os.path.realpath(__file__))


if __name__ == '__main__':

    for ui_file in glob.glob(os.path.join(_cur_dir, '*.ui')):
        name = ntpath.basename(ui_file).split('.')[0]
        py_file = os.path.join(_cur_dir, f'ui_{name}.py')
        os.system(f"pyside2-uic {ui_file} > {py_file}")
        # A bug in PySide2?
        if platform.system() == 'Linux':
            os.system(f"sed -i -- \"s/QString()/''/g\" {py_file}")
        os.remove(ui_file)

    os.system(f"pyside2-rcc -o netpanel_rc.py netpanel.qrc")

    # Fix the path issue of the RC file.
    for rc in glob.glob(os.path.join(_cur_dir, 'ui_*.py')):
        if platform.system() == 'Linux':
            os.system(f"sed -i -- \"s/import netpanel_rc/import ui.netpanel_rc/g\" {rc}")
        else:
            with open(rc, "r") as sources:
                lines = sources.readlines()
            with open(rc, "w") as sources:
                for line in lines:
                    sources.write(line.replace("import netpanel_rc", "import ui.nepanel_rc"))
