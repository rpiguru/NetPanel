# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/pi/NetPanel/ui/mode_config.ui',
# licensing of '/home/pi/NetPanel/ui/mode_config.ui' applies.
#
# Created: Sun Sep 27 11:14:39 2020
#      by: pyside2-uic  running on PySide2 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_ModeConfigWidget(object):
    def setupUi(self, ModeConfigWidget):
        ModeConfigWidget.setObjectName("ModeConfigWidget")
        ModeConfigWidget.resize(920, 496)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(ModeConfigWidget)
        self.verticalLayout_2.setSpacing(50)
        self.verticalLayout_2.setContentsMargins(20, 20, 20, 20)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.lb_title = QtWidgets.QLabel(ModeConfigWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lb_title.sizePolicy().hasHeightForWidth())
        self.lb_title.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setWeight(75)
        font.setBold(True)
        self.lb_title.setFont(font)
        self.lb_title.setAutoFillBackground(True)
        self.lb_title.setStyleSheet("hover {\n"
"    background-color: rgb(255, 255, 255);\n"
"    color: rgb(0, 0, 0);\n"
"}")
        self.lb_title.setObjectName("lb_title")
        self.verticalLayout_2.addWidget(self.lb_title)
        self.frame = QtWidgets.QFrame(ModeConfigWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lb_0 = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.lb_0.setFont(font)
        self.lb_0.setAutoFillBackground(True)
        self.lb_0.setStyleSheet("")
        self.lb_0.setObjectName("lb_0")
        self.verticalLayout.addWidget(self.lb_0)
        self.lb_1 = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.lb_1.setFont(font)
        self.lb_1.setAutoFillBackground(True)
        self.lb_1.setStyleSheet("")
        self.lb_1.setObjectName("lb_1")
        self.verticalLayout.addWidget(self.lb_1)
        self.lb_2 = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.lb_2.setFont(font)
        self.lb_2.setAutoFillBackground(True)
        self.lb_2.setStyleSheet("")
        self.lb_2.setObjectName("lb_2")
        self.verticalLayout.addWidget(self.lb_2)
        self.lb_3 = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.lb_3.setFont(font)
        self.lb_3.setAutoFillBackground(True)
        self.lb_3.setStyleSheet("")
        self.lb_3.setObjectName("lb_3")
        self.verticalLayout.addWidget(self.lb_3)
        self.verticalLayout_2.addWidget(self.frame)

        self.retranslateUi(ModeConfigWidget)
        QtCore.QMetaObject.connectSlotsByName(ModeConfigWidget)

    def retranslateUi(self, ModeConfigWidget):
        ModeConfigWidget.setWindowTitle(QtWidgets.QApplication.translate("ModeConfigWidget", "Form", None, -1))
        self.lb_title.setText(QtWidgets.QApplication.translate("ModeConfigWidget", "Please select a mode", None, -1))
        self.lb_0.setText(QtWidgets.QApplication.translate("ModeConfigWidget", "netPanel", None, -1))
        self.lb_1.setText(QtWidgets.QApplication.translate("ModeConfigWidget", "PC MODE", None, -1))
        self.lb_2.setText(QtWidgets.QApplication.translate("ModeConfigWidget", "PHOTO MODE", None, -1))
        self.lb_3.setText(QtWidgets.QApplication.translate("ModeConfigWidget", "MEDIA MODE", None, -1))

