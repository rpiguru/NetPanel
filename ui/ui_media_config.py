# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/pi/NetPanel/ui/media_config.ui',
# licensing of '/home/pi/NetPanel/ui/media_config.ui' applies.
#
# Created: Sun Sep 27 11:14:39 2020
#      by: pyside2-uic  running on PySide2 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MediaConfigWidget(object):
    def setupUi(self, MediaConfigWidget):
        MediaConfigWidget.setObjectName("MediaConfigWidget")
        MediaConfigWidget.resize(678, 303)
        self.verticalLayout = QtWidgets.QVBoxLayout(MediaConfigWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lb_title = QtWidgets.QLabel(MediaConfigWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lb_title.sizePolicy().hasHeightForWidth())
        self.lb_title.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setWeight(75)
        font.setBold(True)
        self.lb_title.setFont(font)
        self.lb_title.setAutoFillBackground(True)
        self.lb_title.setStyleSheet("hover {\n"
"    background-color: rgb(255, 255, 255);\n"
"    color: rgb(0, 0, 0);\n"
"}")
        self.lb_title.setObjectName("lb_title")
        self.verticalLayout.addWidget(self.lb_title)
        self.widget = QtWidgets.QWidget(MediaConfigWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setObjectName("widget")
        self.layout = QtWidgets.QVBoxLayout(self.widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setObjectName("layout")
        self.verticalLayout.addWidget(self.widget)
        self.btnBack = QtWidgets.QPushButton(MediaConfigWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName("btnBack")
        self.verticalLayout.addWidget(self.btnBack)

        self.retranslateUi(MediaConfigWidget)
        QtCore.QMetaObject.connectSlotsByName(MediaConfigWidget)

    def retranslateUi(self, MediaConfigWidget):
        MediaConfigWidget.setWindowTitle(QtWidgets.QApplication.translate("MediaConfigWidget", "Form", None, -1))
        self.lb_title.setText(QtWidgets.QApplication.translate("MediaConfigWidget", "Please Plug a USB Drive", None, -1))
        self.btnBack.setText(QtWidgets.QApplication.translate("MediaConfigWidget", "Back", None, -1))

