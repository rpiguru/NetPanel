# NetPanel

NetPanel Project with Raspberry Pi

## Components

- Raspberry Pi 3 A+

- MPU6050

    https://core-electronics.com.au/mpu-6050-module-3-axis-gyroscope-acce-lerometer.html

## Installation

- Download the latest **Raspbian Buster Desktop** and flash your micro sd card.

- Clone this repository

    ```shell script
    cd ~
    git clone https://gitlab.com/rpiguru/NetPanel
    ```

- Install everything

    ```shell script
    cd NetPanel
    bash setup.sh
    ```

- And reboot!
