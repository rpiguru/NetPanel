from functools import partial

from PySide2.QtCore import Qt, QTimer
from pynput import keyboard
from utils.settings import MODES, MODE_SELECT_TIMEOUT
from ui.ui_mode_config import Ui_ModeConfigWidget
from utils.common import get_config, update_config_file
from utils.logger import logger
from widgets.base import BaseConfigWidget


class ModeConfigWidget(BaseConfigWidget):

    def __init__(self, parent, name):
        super().__init__(parent, name)
        self.ui = Ui_ModeConfigWidget()
        self.ui.setupUi(self)
        self.parent.setDisabled(False)
        self.parent.setCursor(Qt.ArrowCursor)
        self._cnt = MODE_SELECT_TIMEOUT
        self._timer = QTimer()
        setattr(self, "ModeConfigWidget._on_timeout", self._on_timeout)
        self._timer.timeout.connect(self._on_timeout)
        mode = get_config().get('mode')
        if mode:
            self.index = MODES.index(mode)
            self._set_current_mode()
            self._timer.start(1000)
        else:
            self.index = 0
        self._hover_index = None
        for i in range(4):
            getattr(self.ui, f"lb_{i}").enterEvent = partial(self._on_enter_label, i)
            getattr(self.ui, f"lb_{i}").leaveEvent = partial(self._on_leave_label, i)

    def _on_timeout(self):
        if self._cnt > 0:
            self._cnt -= 1
            self.ui.lb_title.setText(f"Please select a mode  {self._cnt}")
        else:
            logger.info("Mode Select: Timeout")
            self._timer.stop()
            self._confirm_mode()

    def stop_timer(self):
        self._timer.stop()
        self.ui.lb_title.setText("Please select a mode")

    def on_key_pressed(self, key):
        if key == keyboard.Key.down:
            if self.index < len(MODES) - 1:
                self.index += 1
            else:
                self.index = 0
            self._set_current_mode()
            self.stop_timer()
        elif key == keyboard.Key.up:
            if self.index > 0:
                self.index -= 1
            else:
                self.index = len(MODES) - 1
            self._set_current_mode()
            self.stop_timer()
        elif key == keyboard.Key.enter:
            self._confirm_mode()

    def _confirm_mode(self):
        _mode = MODES[self.index]
        logger.info(f"New Mode is selected - {_mode}")
        update_config_file({"mode": _mode})
        if _mode == 'PC MODE':
            self.switch_widget('pin_config')
        elif _mode == 'netPanel':
            self.switch_widget('url_config')
        else:
            self.switch_widget('media_config')

    def _set_current_mode(self):
        for i in range(4):
            label = getattr(self.ui, f'lb_{i}')
            if i == self.index:
                label.setStyleSheet("QLabel { background-color: rgb(255, 255, 255); color: rgb(0, 0, 0)}")
            else:
                label.setStyleSheet("QLabel { background-color: rgba(0, 0, 0, 0); color: rgb(255, 255, 255)}")

    def _on_enter_label(self, index, event):
        getattr(self.ui, f"lb_{index}").setStyleSheet(
            "QLabel { background-color: rgb(255, 255, 255); color: rgb(0, 0, 0)}")

    def _on_leave_label(self, index, event):
        if index != self.index:
            getattr(self.ui, f"lb_{index}").setStyleSheet(
                "QLabel { background-color: rgba(0, 0, 0, 0); color: rgb(255, 255, 255)}")

    def mouseReleaseEvent(self, event):
        for i in range(4):
            if getattr(self.ui, f"lb_{i}").underMouse():
                self.index = i
                self._confirm_mode()
                break
        super(ModeConfigWidget, self).mouseReleaseEvent(event)
