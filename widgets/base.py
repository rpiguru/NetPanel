from PySide2.QtCore import QTimer
from PySide2.QtWidgets import QWidget
from pynput import keyboard

from utils.settings import PIN_SELECT_TIMEOUT
from utils.logger import logger
from widgets.dlg import show_error_message


class BaseConfigWidget(QWidget):

    def __init__(self, parent, name):
        super().__init__(parent)
        self.parent = parent
        self.name = name

    def switch_widget(self, name, **kwargs):
        cur_widget_name = getattr(self.parent, "_cur_widget_name")
        if self.name != cur_widget_name:
            logger.warning(f"Called from wrong widget({self.name}). Current: {cur_widget_name},")
            return
        self.parent.switch_widget(name, **kwargs)

    def on_key_pressed(self, key):
        pass


class InputWidgetBase(BaseConfigWidget):

    def __init__(self, parent, ui_cls, name):
        super().__init__(parent, name)
        self.ui = ui_cls()
        self.ui.setupUi(self)
        for func in {'on_btn_submit', 'on_btn_back', '_on_text_changed', '_check_timer'}:
            setattr(self, f"InputWidgetBase.{func}", getattr(self, func))
        self.ui.btnSubmit.released.connect(self.on_btn_submit)
        self.ui.btnBack.released.connect(self.on_btn_back)
        self._timer = QTimer()
        self._cnt = PIN_SELECT_TIMEOUT
        self._timer.timeout.connect(self._check_timer)
        self._timer.start(1000)
        self._init_txt()
        self.ui.txt.setFocus()
        self.ui.txt.textChanged.connect(self._on_text_changed)

    def _init_txt(self):
        pass

    def stop_timer(self):
        if self._timer is not None:
            self._timer.stop()
            self._timer = None

    def _on_text_changed(self, *args):
        if self._timer is not None:
            self.ui.lb_title.setText(" ".join(self.ui.lb_title.text().split()[:-1]))
        self.stop_timer()

    def _check_timer(self):
        pass

    def on_key_pressed(self, key):
        if key == keyboard.Key.enter:
            if self.ui.txt.hasFocus():
                self.on_btn_submit()

    def on_btn_submit(self, show_error=True):
        val = self.ui.txt.text()
        if not val:
            if show_error:
                show_error_message("Please input correct value!")
        else:
            self._process_input(val, show_error=show_error)

    def _process_input(self, val, show_error):
        pass

    def on_btn_back(self):
        self.switch_widget('mode')

    def closeEvent(self, *args, **kwargs):
        self.stop_timer()
