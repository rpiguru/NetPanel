from PySide2.QtWidgets import QMessageBox


def show_error_message(msg):
    m = QMessageBox()
    m.setWindowTitle("NetPanel")
    m.setIcon(m.Icon.Warning)
    m.setText(msg)
    m.exec_()
