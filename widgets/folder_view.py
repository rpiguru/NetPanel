from PySide2.QtCore import QDir
from PySide2.QtWidgets import QTreeView, QFileSystemModel


class FolderTreeView(QTreeView):

    def __init__(self, parent, path):
        super().__init__(parent)
        self._model = QFileSystemModel()
        self._model.setFilter(QDir.AllDirs | QDir.NoDotAndDotDot | QDir.Dirs)
        self.setModel(self._model)
        self._model.setRootPath(path)
        self.setRootIndex(self._model.index(path))
        setattr(self, "FolderTreeView._fetch_and_expand", self._fetch_and_expand)
        self._model.directoryLoaded.connect(self._fetch_and_expand)

    def _fetch_and_expand(self, path):
        index = self._model.index(path)
        self.expand(index)  # expand the item
        for i in range(self._model.rowCount(index)):
            # fetch all the sub-folders
            child = index.child(i, 0)
            if self._model.isDir(child):
                self._model.setRootPath(self._model.filePath(child))
