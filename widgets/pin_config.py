from ui.ui_pin_config import Ui_PinConfig
from utils.common import get_config, update_config_file, check_internet_connection
from utils.logger import logger
from widgets.base import InputWidgetBase
from widgets.dlg import show_error_message


class PINConfigWidget(InputWidgetBase):

    def __init__(self, parent, name):
        super().__init__(parent, ui_cls=Ui_PinConfig, name=name)
        for func in {'on_btn_submit', 'on_btn_back', '_on_text_changed', '_check_timer'}:
            setattr(self, f"PINConfigWidget.{func}", getattr(self, func))

    def _init_txt(self):
        conf = get_config()
        if conf.get('authorized'):
            self.ui.txt.setText(conf['pin'])
        else:
            self.stop_timer()

    def _check_timer(self):
        if self._cnt > 0:
            self.ui.lb_title.setText(f"Please input PIN {self._cnt}")
            self._cnt -= 1
        else:
            self.stop_timer()
            self.on_btn_submit(show_error=False)

    def _process_input(self, val, show_error):
        if val == get_config().get('pin'):
            self.ui.lb_title.setText(f"Starting PIN Mode")
            logger.debug("PC Mode started!")
            self.stop_timer()
            update_config_file({'authorized': True})
            if not self.parent.is_internet_required() or check_internet_connection():
                self.parent.hide_me()
            if self.parent.is_internet_required():
                self.parent.start_internet_monitoring()
        else:
            if show_error:
                show_error_message("Please input correct PIN!")
