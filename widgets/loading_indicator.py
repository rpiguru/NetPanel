from PySide2.QtCore import Qt
from PySide2.QtCore import QByteArray
from PySide2.QtGui import QMovie
from PySide2.QtWidgets import QWidget, QLabel, QSizePolicy, QVBoxLayout


class LoadingIndicator(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.movie = QMovie("images/loader.gif", QByteArray(), self)
        self.setGeometry(parent.width() // 2 - 30, parent.height() // 2 - 30, 60, 60)
        self.movie_screen = QLabel()
        self.movie_screen.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.movie_screen.setAlignment(Qt.AlignCenter)

        # Create the layout
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.movie_screen)

        self.setLayout(main_layout)

        # Add the QMovie object to the label
        self.movie.setCacheMode(QMovie.CacheAll)
        self.movie.setSpeed(100)
        self.movie_screen.setMovie(self.movie)
        self.movie.start()
