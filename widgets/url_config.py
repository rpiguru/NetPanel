import time

import psutil
from PySide2.QtCore import QTimer

from ui.ui_url_config import Ui_URLConfig
from utils.common import get_config, update_config_file, check_internet_connection
from utils.logger import logger
from widgets.base import InputWidgetBase
from widgets.dlg import show_error_message


class URLConfigWidget(InputWidgetBase):

    def __init__(self, parent, name):
        super().__init__(parent, ui_cls=Ui_URLConfig, name=name)
        for func in {'on_btn_submit', 'on_btn_back', '_on_text_changed', '_check_timer', '_check_internet_and_start'}:
            setattr(self, f"URLConfigWidget.{func}", getattr(self, func))
        self._i_timer = QTimer()
        self._i_timer.timeout.connect(self._check_internet_and_start)
        self._start_time = 0

    def _init_txt(self):
        url = get_config().get('url')
        if url:
            self.ui.txt.setText(url)
        else:
            self.stop_timer()

    def _check_timer(self):
        if self._cnt > 0:
            self.ui.lb_title.setText(f"Please input URL {self._cnt}")
            self._cnt -= 1
        else:
            self.stop_timer()
            self.on_btn_submit()

    def _process_input(self, val, show_error):
        if "netpanel.tech" not in val:
            if show_error:
                show_error_message("Invalid URL!")
        else:
            logger.info(f"netPanel Mode is started!, new URL - {val}")
            self.stop_timer()
            update_config_file({'url': val})
            self.ui.lb_title.setText("Opening Browser...")
            self._start_time = time.time()
            self._i_timer.start(1000)

    def _check_internet_and_start(self):
        # Give 10 sec limit until the internet is available. DNS error on Scott's pi!?
        if check_internet_connection():
            self.parent.open_browser()
            self._i_timer.stop()
        elif time.time() - psutil.boot_time() > 10:
            self.parent.start_internet_monitoring()
            self._i_timer.stop()

    def closeEvent(self, *args, **kwargs):
        self._i_timer.stop()
        self._i_timer = None
        super(URLConfigWidget, self).closeEvent(*args, **kwargs)
