import glob
import os
import subprocess
import time
from pynput import keyboard
import vlc
from PySide2.QtCore import QTimer, Qt

from utils.settings import PATH_SELECT_TIMEOUT, USB_PATH, PHOTO_CMD
from ui.ui_media_config import Ui_MediaConfigWidget
from utils.common import update_config_file, get_config, is_video_file, check_running_proc
from utils.logger import logger
from widgets.base import BaseConfigWidget
from widgets.folder_view import FolderTreeView
from widgets.loading_indicator import LoadingIndicator


class MediaConfigWidget(BaseConfigWidget):

    _video_files = []
    _video_index = -1

    def __init__(self, parent, name):
        super().__init__(parent, name)
        self.ui = Ui_MediaConfigWidget()
        self.ui.setupUi(self)
        for func in {'_check_usb_and_show_tree_view', '_check_media_path', '_check_media_play',
                     '_on_btn_back', '_on_btn_path'}:
            setattr(self, f"MediaConfigWidget.{func}", getattr(self, func))
        self._usb_timer = QTimer()
        self._usb_timer.timeout.connect(self._check_usb_and_show_tree_view)
        self._cnt = 0
        self._path_timer = QTimer()
        self._path_timer.timeout.connect(self._check_media_path)
        self._media_timer = QTimer()
        self._media_timer.timeout.connect(self._check_media_play)
        self._media_path = None
        self._is_paused = False
        self.tree = None
        # Initialize media player
        self._instance = vlc.Instance(['--video-on-top'])
        self._player = self._instance.media_player_new()
        self._player.set_fullscreen(True)
        self.ui.btnBack.released.connect(self._on_btn_back)
        self.initialize()

    def initialize(self):
        self.setCursor(Qt.ArrowCursor)
        self._media_timer.stop()
        self._media_path = None
        self.tree = None
        self._clear_layout()
        self.ui.lb_title.setText("Please Plug a USB Device")
        self.ui.layout.addWidget(LoadingIndicator(self))
        if not self._check_usb_and_show_tree_view():
            self._usb_timer.start(1000)

    @staticmethod
    def get_connected_usb_path():
        for path in glob.glob(USB_PATH):
            if os.path.isdir(path) and [f for f in os.listdir(path) if f != '.created_by_pmount']:
                return path

    def _check_usb_and_show_tree_view(self):
        path = self.get_connected_usb_path()
        if path:
            self._usb_timer.stop()
            self.ui.lb_title.setText("Device Connected!")
            logger.info(f"USB Device connected - {path}")
            self._media_path = path
            self.tree = FolderTreeView(parent=self, path=path)
            self.tree.doubleClicked.connect(self._on_btn_path)
            self._clear_layout()
            self.ui.layout.addWidget(self.tree)
            self._cnt = PATH_SELECT_TIMEOUT
            self._path_timer.start(1000)
            return True

    def _clear_layout(self):
        for i in reversed(range(self.ui.layout.count())):
            item = self.ui.layout.itemAt(i)
            if item.widget() is not None:
                item.widget().setParent(None)

    def _check_media_path(self):
        self.ui.lb_title.setText(f"Please select a folder  {self._cnt}")
        if self._cnt > 0:
            self._cnt -= 1
        else:
            logger.info(f"Timeout from Media Path Selector, using default - {self._media_path}")
            self._start_media()

    def _on_btn_path(self, *args):
        index = self.tree.currentIndex()
        path = self.tree.model().filePath(index)
        logger.info(f"Selected folder - {path}")
        self._media_path = path
        self._start_media()

    def _start_media(self):
        self._path_timer.stop()
        update_config_file({'media_path': self._media_path})
        self.setCursor(Qt.BlankCursor)
        if get_config().get('mode') == 'PHOTO MODE':
            cmd = PHOTO_CMD.format(path=self._media_path)
            logger.info(f"Playing PHOTO mode: {cmd}")
            subprocess.Popen(cmd, shell=True)
        else:
            self._clear_layout()
            self.ui.lb_title.setText("Loading Video...")
            self._video_files = [f for f in glob.glob(self._media_path + '/**/*.*', recursive=True) if is_video_file(f)]
            self._video_index = -1
            logger.debug(f"Video files: {self._video_files}")
        self._media_timer.start(1000)

    def _check_media_play(self):
        if get_config().get('mode') == 'PHOTO MODE':
            if not check_running_proc('feh'):
                logger.info(f"Photo mode is closed")
                self.initialize()
        else:
            if not self._player.is_playing() and not self._is_paused:
                if self._video_files:
                    if self._video_index < len(self._video_files) - 1:
                        self._video_index += 1
                    else:
                        self._video_index = 0
                    self._play_video()

    def _play_video(self):
        try:
            media_file = self._video_files[self._video_index]
            logger.info(f"=== Playing {self._video_index} {media_file}")
            media = self._instance.media_new(media_file)
            self._player.set_media(media)
            self._player.play()
            time.sleep(3)
        except Exception as e:
            logger.error(f"Failed to play - {e}")

    def on_key_pressed(self, key):
        if self._media_path is None:
            self._path_timer.stop()
            self.ui.lb_title.setText("Please select a folder")
            # TODO: Navigate tree widget with arrow keys here
            return
        if get_config().get('mode') == 'MEDIA MODE':
            if key == keyboard.Key.esc:
                self._player.stop()
                self._media_timer.stop()
                self.initialize()
            elif key == keyboard.Key.space:
                self._player.set_pause(0 if self._is_paused else 1)
                self._is_paused = not self._is_paused
            elif key == keyboard.Key.right:
                self._player.stop()
                if self._video_index < len(self._video_files) - 1:
                    self._video_index += 1
                else:
                    self._video_index = 0
                self._play_video()
            elif key == keyboard.Key.left:
                self._video_index = self._video_index - 1 if self._video_index > 1 else len(self._video_files) - 1
                self._play_video()

    def closeEvent(self, *args, **kwargs):
        if self._media_path is not None:
            self._path_timer.stop()
        super(MediaConfigWidget, self).closeEvent(*args, **kwargs)

    def _on_btn_back(self):
        self.switch_widget('mode')
