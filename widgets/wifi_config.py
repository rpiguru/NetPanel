import threading
import time

from PySide2.QtCore import Signal, Qt
from pynput import keyboard

from ui.ui_wifi_config import Ui_WiFiConfigWidget
from utils.common import scan_wifi_networks, start_ap, create_wifi_config, start_station, get_config
from utils.logger import logger
from widgets.base import BaseConfigWidget
from widgets.dlg import show_error_message


class WiFiConfigWidget(BaseConfigWidget):

    on_scanned = Signal(list)

    def __init__(self, parent, name):
        super().__init__(parent, name)
        self.ui = Ui_WiFiConfigWidget()
        self.ui.setupUi(self)
        for func in {'_on_btn_connect', '_on_btn_skip', '_on_btn_back', '_on_wifi_scanned'}:
            setattr(self, f"WiFiConfigWidget.{func}", getattr(self, func))
        start_ap()
        self.setCursor(Qt.ArrowCursor)
        self.ui.btnConnect.released.connect(self._on_btn_connect)
        if get_config()['mode'] == 'netPanel':
            self.ui.btnSkip.deleteLater()
        else:
            self.ui.btnSkip.released.connect(self._on_btn_skip)
        self.ui.btnBack.released.connect(self._on_btn_back)
        self.on_scanned.connect(self._on_wifi_scanned)
        self._b_stop = False
        self._tr = threading.Thread(target=self._scan_wifi)
        self._tr.start()

    def _scan_wifi(self):
        time.sleep(2)   # Important!!!
        while not self._b_stop:
            ap_list = scan_wifi_networks()
            try:
                self.on_scanned.emit(ap_list)
            except RuntimeError:
                break
            time.sleep(2)

    def _on_wifi_scanned(self, ap_list):
        if self._b_stop:
            return
        combo = self.ui.comboSSID
        cur_ssid = combo.currentText()
        combo.clear()
        combo.addItems(ap_list)
        if cur_ssid and cur_ssid in ap_list:
            combo.setCurrentText(cur_ssid)

    def on_key_pressed(self, key):
        if key == keyboard.Key.enter:
            if self.ui.txtPassword.hasFocus():
                self._on_btn_connect()

    def _on_btn_connect(self):
        ssid = self.ui.comboSSID.currentText()
        pwd = self.ui.txtPassword.text()
        if len(pwd) < 8 or not ssid:
            show_error_message("Please input correctly!")
            return
        self._b_stop = True
        logger.info(f"Connecting to AP - {ssid}")
        self.ui.lb_title.setText(f"Connecting to {ssid}")
        self.setDisabled(True)
        create_wifi_config(ssid=ssid, password=pwd)
        self.parent.save_wifi_connect_time()
        start_station()

    def _on_btn_skip(self):
        self.parent.disable_internet()
        self.parent.hide_me()

    def _on_btn_back(self):
        self.switch_widget('mode')

    def closeEvent(self, event):
        self._b_stop = True
        super(WiFiConfigWidget, self).closeEvent(event)

    def hideEvent(self, event):
        self._b_stop = True
        super(WiFiConfigWidget, self).hideEvent(event)
