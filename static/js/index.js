function set_mode(mode) {
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/set_mode?mode=" + mode, false);
    xhttp.send();
    let resp = xhttp.responseText;
    if (resp === "OK"){
        alert("Successfully Set Mode!");
    }
}


const pin = document.getElementById("pin");
pin.addEventListener("keyup", function(event) {
  if (event.defaultPrevented) {
    return; // Should do nothing if the default action has been cancelled
  }
  // Number 13 is the "Enter" key on the keyboard
  if (event.which === 13 || event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("btn_submit").click();
  }
});


const pwd = document.getElementById("pin");
pwd.addEventListener("keyup", function(event) {
  if (event.defaultPrevented) {
    return; // Should do nothing if the default action has been cancelled
  }
  // Number 13 is the "Enter" key on the keyboard
  if (event.which === 13 || event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("btn_submit_wifi").click();
  }
});
